# Theoretical Computer Science

## Le plus grand commun diviseur d'Euclide

Bienvenue au challenge d'informatique théorique. Vous devez construire un automate déterministe à pile qui résout l'algorithme du plus grand commun diviseur d'Euclide pour des paramètres binaires `m` et `n` inscrits dans la pile de la façon suivante:

e.g. : Pour `m = 8 (binaire: 1000)` et `n = 6 (binaire: 110)`, on aura la pile: `110#1000#` (On dépile/empile à gauche et `#` est un caractère délimiteur faisant partie du langage accepté par la pile, c.-à-d. `{0,1,#}`.)

Le pseudo-code de l'algorithme d'Euclide est le suivant:

```
while n > 0 do
    r ← m mod n
    m ← n
    n ← r
return m
```

### Consignes supplémentaires

Pour simplifier le problème, nous vous demandons de créer l'automate valide que pour les cas où `0 < n ≤ 3`.

La syntaxe pour les transitions de votre automate doit être la suivante: `ce qui est lu; ce qui est dépilé, ce qui est empilé`.

Utilisez ce symbole `ε` pour indiquer l'absence de caractère.

Assurez vous de bien indiquer l'état initial par une flèche et nommez tous les états finaux par le nombre correspondant au plus grand commun diviseur trouvé par l'automate. Il peut y avoir plusieurs états finaux se terminant avec la même valeur.

Pour simplifier l'automate, vous avez le droit d'empiler plusieurs symboles à la fois.

e.g. : Si la pile contient: `#11#`, après la transition `#; ε, 001` on aura: `100#11#`.

Rappel, l'automate doit être DÉTERMINISTE.

### Modalités de remises

Un template Latex avec toutes les commandes nécessaires pour construire l'automate est fourni avec un exemple. Vous pouvez ou non l'utiliser à votre guise. Tant que ce qui est remis est un fichier PDF et que son contenu est lisible.
Vous pourriez également utiliser Graphviz en ligne: http://www.webgraphviz.com/ (voir sample 4)


### Indices

Pensez l'automate en trois parties distinctes: un aiguillage vers le bon modulo (1, 2 ou 3), le modulo comme tel et les branchements pour les étapes subséquentes d'Euclide.

L'algorithme d'Euclide peut être décrit comme une récursion où les appels se font ainsi: `Euclide(m, n) → Euclide(n, m mod n) → Euclide(m mod n, n mod (m mod n)) → …` avec la condition de sortie quand `n = 0` ou `n = 1` (car le PGCD d'un nombre et 1 sera toujours 1).


Bonne chance !
