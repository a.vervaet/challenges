INGRÉDIENTS

- 8 pilons de poulet avec ou sans la peau

- 500 ml (2 tasses) de lait de beurre

- 210 g (1 1/2 tasse) de farine tout usage non blanchie

- 5 ml (1 c. à thé) de poudre à pâte

- 5 ml (1 c. à thé) de paprika

- 5 ml (1 c. à thé) de poivre de Cayenne

- 5 ml (1 c. à thé) de poudre d’ail

- 5 ml (1 c. à thé) de poudre d’oignon

- 5 ml (1 c. à thé) de moutarde sèche

- 5 ml (1 c. à thé) de sel

- 675 g (3 tasses) de graisse végétale, pour la friture

PRÉPARATION

1. Dans un bol, mélanger le poulet et le lait de beurre. Couvrir et réfrigérer 12 heures.

2. Dans un autre bol, mélanger la farine, la poudre à pâte, les épices et le sel.

3. Retirer le poulet du lait de beurre. Enrober les pilons du mélange de farine. Secouer pour retirer l’excédent. Déposer sur une plaque. Une fois tous les pilons panés, tremper une seconde fois dans le lait de beurre, puis enrober à nouveau dans le mélange de farine.

4. Préchauffer la graisse végétale dans la friteuse à 185 °C (365 °F). Tapisser une plaque de cuisson de papier absorbant.

5. Déposer la moitié des pilons de poulet à la fois dans la graisse chaude et les cuire environ 15 minutes ou jusqu’à ce qu’un thermomètre inséré au centre d’un pilon sans toucher l’os indique 82 °C (180 °F), en les retournant à quelques reprises durant la cuisson. Attention aux éclaboussures. Égoutter sur le papier absorbant. Saler.
