# Puzzle Heroes

## Deux mots

On veut trouver l'algorithme le plus performant pour le problème qui suit. On vous passe deux mots contenant les caractères a à z en minuscule. On cherche à savoir si les deux mots passés contiennent exactement le même nombre de lettres et avec la même fréquence.

exemple: lorsque l'algorithme reçoit:

* `"allo"` et `"lola"`, retourne `true`
* `"allo"` et `"aaaallllloooo"`, retourne `false`
* `"allo"` et `"polo"`, retourne `false`

### Consignes

* L'algorithme doit s'exécuter en O(n) où n = nombre de lettres du mot 1 + nombre de lettres du mot 2
* L'espace mémoire utilisée doit être en O(1)

### Astuces
* Que se passe-t-il lorsqu'on passe un mot qui contient deux fois la même lettre à votre algo?
* Quels sont les temps d'exécution des opérations sur vos structures de données utilisées?
* Plusieurs boucles consécutives qui ont un temps d'exécution linéaire donne un algo ayant un temps d'exécution linéaire.

Rappel: La règle du maximum

```
Si f1(n) ∈ Θ(g1(n))
et f2(n) ∈ Θ(g2(n)),

alors

f1(n) + f2(n) ∈ Θ(max(g1(n), g2(n))).

Exemple: n + nlog(n) ∈ Θ(n log n)
```

### Réponse
Vous devez écrire votre pseudo-code dans ce README. Vous pouvez écrire votre démarche textuellement pour expliquer votre code et comment fait-il pour s'exécuter en O(n) et avoir O(1) en mémoire.

```
votre code ici

### Code

IF mot1.length == mot2.length DO												
	Compteur = Array(size = 26, defaultValue = 0)
	FOR ( letter in mot1) DO
		Compteur[letter.valueInASCII - 97] += 1
	END FOR
	FOR ( letter in mot2) DO
		Compteur[letter.valueInASCII - 97] -= 1
	END FOR
	IF Compteur == Array(size = 26, defaultValue = 0) DO
		RETURN true
	END IF
END IF
RETURN false

### Explication
Le premier if sert a filtrer directement les mots n'ayant pas le même nombre de lettres pour un meilleur cas en O(1)
Le but de ce code est de creér un tableau de référence qui nous permettra de compter nos occurences de lettres, il comptient une case par lettre de l'alphabet.
On incrémente la case correspondant a la lettre en balayant le mot1 et on décremente pour le mot2
Si on fini avec un tableau vide ( rempli de 0) alors les occurence étaient les même et on renvoie true sinon false

Les deux for s'executent respectivement en o(n) car les mots ont la meme taille 
on se retrouve avec une complexite final en 0(2n) ~ O(n)
Et une utilisation memoire en O(1) car les mots ne sont pas copiés et la taille du tableau ne change pas selon n
```
