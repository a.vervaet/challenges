import turtle


class Map():


    def __init__(self, numero, ts=25):
        self.tilesize = ts

        chemin_du_niveau = "TMen-map{}.txt".format(numero)
        with open(chemin_du_niveau, "r") as file:
            self.map = file.read()
        with open(chemin_du_niveau, "r") as file:
            self.evaluation = file.readlines()
            self.hauteur = len(self.evaluation) + 1
            self.largeur = len(self.evaluation[0]) - 1

        self.scr = turtle.Screen()
        self.scr.setup(self.largeur * self.tilesize, (self.hauteur) * self.tilesize)
        self.scr.bgcolor('#000000')
        self.scr.setworldcoordinates(0, (self.hauteur - 1) * self.tilesize, self.largeur * self.tilesize,
                                     - self.tilesize)
        self.scr.tracer(0)
        self.wall = turtle.Turtle()
        self.wall.ht()
        self.food = turtle.Turtle()
        self.food.ht()
        self.food.up()
        self.food_map = []
        self.create_map()
        self.create_food_map()
        turtle.ontimer(self.run(), 0)
        turtle.mainloop()

    def create_map(self):
        pos_y = 0
        for pos_x in range(len(self.map)):
            if self.map[pos_x] == 'X':
                self.wallCreator((pos_x - (pos_y * (self.largeur + 1))) * self.tilesize,
                                 y = (pos_y) * self.tilesize)
            if self.map[pos_x] == '\n':
                pos_y += 1

    def create_food_map(self):
        for pos_y in range(self.hauteur):
            self.food_map.append([""])        
            for pos_x in range(self.largeur + 1):            
                self.food_map[pos_y].append(False if self.map[pos_x + (pos_y * (self.largeur - 1))] == '.' else True)

    def print_food(self):
        for i in range(len(self.food_map)):
            for j in range(len(self.food_map[0])):
                if self.food_map[i][j]:
                    food_pos = turtle.Vec2D((j * self.tilesize), (i * self.tilesize))
                    self.food.goto(food_pos)
                    self.food.dot(8, 'yellow')

    def run(self):
        self.print_food()
        self.scr.update()
        turtle.ontimer(self.run(), 20)

    def wallCreator(self, x, y=0):
        self.wall.home()
        self.wall.up()
        self.wall.goto(x, y)
        self.wall.lt(90)
        self.wall.fd(self.tilesize / 2)
        self.wall.lt(90)
        self.wall.fd(self.tilesize / 2)
        self.wall.setheading(0)
        self.wall.fillcolor('#2ebbff')  # f2f8f7, 1ecffa, 2ebbff, 8f1aff
        self.wall.begin_fill()
        self.wall.down()
        self.wall.fd(self.tilesize)
        self.wall.lt(-90)
        self.wall.fd(self.tilesize / 3)
        self.wall.up()
        self.wall.fd(self.tilesize / 3)
        self.wall.down()
        self.wall.fd(self.tilesize / 3)
        self.wall.lt(-90)
        self.wall.fd(self.tilesize)
        self.wall.lt(-90)
        self.wall.fd(self.tilesize / 3)
        self.wall.up()
        self.wall.fd(self.tilesize / 3)
        self.wall.down()
        self.wall.fd(self.tilesize / 3)
        self.wall.end_fill()
        self.wall.up()
        self.wall.lt(180)
        self.wall.fd(self.tilesize / 3)
        self.wall.lt(90)
        self.wall.down()
        self.wall.fd(self.tilesize / 2)
        self.wall.lt(90)
        self.wall.fd(self.tilesize / 3)
        self.wall.up()
        self.wall.lt(180)
        self.wall.fd(self.tilesize / 3)
        self.wall.lt(90)
        self.wall.down()
        self.wall.fd(self.tilesize / 2)
        self.wall.up()
        self.wall.lt(-90)
        self.wall.fd(self.tilesize / 3)
        self.wall.lt(-90)
        self.wall.down()
        self.wall.fd(self.tilesize / 4)
        self.wall.lt(-90)
        self.wall.fd(self.tilesize / 3)
        self.wall.up()
        self.wall.lt(180)
        self.wall.fd(self.tilesize / 3)
        self.wall.lt(-90)
        self.wall.down()
        self.wall.fd(self.tilesize / 2)
        self.wall.lt(-90)
        self.wall.fd(self.tilesize / 3)
        self.wall.up()
        self.wall.lt(180)
        self.wall.fd(self.tilesize / 3)
        self.wall.lt(-90)
        self.wall.down()
        self.wall.fd(self.tilesize / 4)
        self.wall.up()
        self.wall.lt(90)
        self.wall.fd(self.tilesize / 3)
        self.wall.lt(90)
        self.wall.fd(self.tilesize / 2)
        self.wall.lt(90)
        self.wall.down()
        self.wall.fd(self.tilesize / 3)
        self.wall.up()
        self.wall.lt(90)
        self.wall.fd(self.tilesize / 2)
        self.wall.lt(-90)
        self.wall.fd((2 * self.tilesize) / 3)

    def getMap(self):
        return self.map