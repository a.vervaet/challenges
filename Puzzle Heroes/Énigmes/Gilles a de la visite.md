# Puzzle Heroes

## Gilles reçoit de la visite

### Mise en situation

Gilles reçoit de la visite pour souper. Il a préparé une délicieuse lasagne. Son cousin Paul le mathématicien arrive avec une douzaine de caisses de 24 oeufs.

**Gilles**: Paul! Quel plaisir de te voir! Tu te souviens de mes trois chiens Pirouette, Coconut et Claude?

**Paul**: Gilles, ça faisait si longtemps! Wow, je vois que tu as encore tes animaux depuis la dernière fois! Mais ça fait combien de temps que tu les as dis donc?

**Gilles**: Oui, et ils sont encore en santé! Je te propose un petit jeu pour deviner leurs âges afin de détendre l'atmosphère.

**Paul**: Vas-y.

(L'ambiance était déjà à son comble)

**Gilles**:

* Le produit de l'âge des trois chiens est de 36.

**Paul**: ... je ne sais pas.

**Gilles**:

* La somme de leurs âges est égale au nombre de minutes sur l'horloge dans exactement 5 minutes.

**Paul**: ... c'est difficile à dire.

**Gilles**:

* Mon plus vieux chien préfère les croquettes de poulet aux croquettes de poisson.

**Paul**: AH! C'est facile j'ai trouvé!

### Instructions
Vous devez trouver l'âge des trois chiens de Gilles. Écrivez votre démarche et votre réponse dans ce README.

### Indice
Aucun détail de la mise en scène n'est superflu.

### Réponse
Écrivez votre réponse ici:

Commen9ons par decouper 36 en produit de nombre premier 36 = 3*2*3*2
le premier indice nous laisse donc 8 possibilites d'age : 6*3*2, 9*2*2,4*3*3, 36*1*1, 18*2*1,12*3*1, 1*4*9,1*6*6
Le deuxieme indice elimine nous dit qu'qu mois deux resultats sont possible sinon on aurait trouvé, on a donc le choix entre 1*6*6 et 9*2*2 car leur somme fait 13,
toutes les autres sommes on des résultats différents
Le troisième indice clot l'énigme car on sait qu'il y a un plus vieux la réponse est donc 9 ans 2 ans et 2 ans
