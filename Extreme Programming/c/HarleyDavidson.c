#include <stdlib.h>
#include <time.h>
#include "HarleyDavidson.h"

#define HARLEYDAVIDSON_FUEL_TANK_CAPACITY 20

static void startEngine(void) {/* Call the engine and start it */}
static void stopEngine(void) {/* Call the engine and stop it */}
static void setSpeed(short speedInKmh) {/* Call the wheels and set their speed to this value */}
static void steer(short angleInDegrees) {/* Call the wheels and change their orientation accordingly */}
static void setFlashers(_Bool status) {/* Call the flashers and change their state accordingly */}
static unsigned int getAmountOfFuelInL(void)
{
    // Call the fuel tank and ask for the amount of fuel it contains
    srand(time(NULL));
    return rand() % HARLEYDAVIDSON_FUEL_TANK_CAPACITY;
}

static struct VehicleInterface setDefaultVehiculeCommandsTargets(void)
{
    struct VehicleInterface vehicleCommands = {
        .startEngine = &startEngine,
        .stopEngine = &stopEngine,
        .setSpeed = &setSpeed,
        .steer = &steer,
        .setFlashers = &setFlashers,
        .getAmountOfFuelInL = &getAmountOfFuelInL,
    };
    return vehicleCommands;
}

struct HarleyDavidson *HarleyDavidson_new(void)
{
    struct ReferencedObject *newReferencedObject = ReferencedObject_new();
    struct VehicleInterface newVehicleCommands = setDefaultVehiculeCommandsTargets();

    struct HarleyDavidson *pointer = malloc(sizeof(struct HarleyDavidson));

    pointer->referencedObject = newReferencedObject;
    pointer->vehicleCommands = newVehicleCommands;

    return pointer;
}

void HarleyDavidson_delete(struct HarleyDavidson *harleyDavidson)
{
    if(ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(harleyDavidson->referencedObject)) {
        free(harleyDavidson);
    }
}

void HarleyDavidson_startEngine(struct HarleyDavidson *harleyDavidson)
{
    (*(harleyDavidson->vehicleCommands.startEngine))();
}

void HarleyDavidson_stopEngine(struct HarleyDavidson *harleyDavidson)
{
    (*(harleyDavidson->vehicleCommands.stopEngine))();
}

void HarleyDavidson_setSpeed(struct HarleyDavidson *harleyDavidson, short speedInKmh)
{
    (*(harleyDavidson->vehicleCommands.setSpeed))(speedInKmh);
}

void HarleyDavidson_steer(struct HarleyDavidson *harleyDavidson, short angleInDegrees)
{
    (*(harleyDavidson->vehicleCommands.steer))(angleInDegrees);
}

void HarleyDavidson_setFlashers(struct HarleyDavidson *harleyDavidson, _Bool status)
{
    (*(harleyDavidson->vehicleCommands.setFlashers))(status);
}

unsigned int HarleyDavidson_getAmountOfFuelInL(struct HarleyDavidson *harleyDavidson)
{
    return (*(harleyDavidson->vehicleCommands.getAmountOfFuelInL))();
}

