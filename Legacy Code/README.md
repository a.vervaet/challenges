# Legacy code

## Servi avec sauce à l'italienne aux boulettes de viande

### Pré-requis
* Avoir Java jdk 1.7+ installé sur votre machine
* S'assurer de pouvoir rouler le compilateur java `javac` en ligne de commande ou configurer votre environnement de développement afin de pouvoir builder le projet correctement. (Voir section "Démarrer l'application")

### Instruction
On vous donne le code d'un clone du jeu Terraria (source: https://github.com/raxod502/TerrariaClone). Ce code est un très bon exemple d'une application judicieuse des principes S.O.L.I.D.

Un petit rappel sur ces principes en questions:
* **S**uicide
* **O**mg
* **L**et me die
* **I** have seen things
* **D**ead inside

### Démarrer l'application
* Compiler le code: `javac -d out src/*.java`
* Démarrer l'application: `java -cp out:textures TerrariaClone`

### But
Le but du challenge est d'ajouter une feature au code legacy en la faisant la plus clean possible malgré tout. Vous devrez aussi faire des tests de régression afin de vous assurer que rien n'est brisé!
* Travailler dans du code mal fait
* Ajouter une feature en respectant les bonnes pratiques de programmation
* Faire des tests de régression

### La feature à ajouter
Dans Terraria, vous incarnez un petit personnage qui doit évoluer dans un monde 2D généré procéduralement. Le but du jeu est d'explorer l'univers afin de trouver de quoi se construire une base, crafter des items et de faire évoluer votre monde en battant des boss.

Lorsque vous créez votre personnage dans Terraria Clone, il est créé avec un certain nombre d'items dans son inventaire. Chaque personnage commence la partie avec une petite pioche dans la première case de son inventaire.

La feature à implémenter est tout simplement de faire en sorte que l'item de départ de votre personnage dans la première case de son inventaire soit remplacé par une barre d'uranium en commençant la partie.

Bonne chance!
