# Puzzle Heroes

## Gratte-Ciel

### Mise en situation
On a un Gratte-Ciel de 300 étages. On a trois melons d'eau de même consistance et de même densité. 
Le but est de trouver le premier étage à partir duquel, lorsqu'on le lance par la fenêtre, 
un melon d'eau va éclater en tombant sur le sol. Lorsqu'un melon ne casse pas, 
on peut aller le chercher et le réutiliser. Si le melon n'éclate pas en tombant, 
il n'est aucunement endommagé. Lorsqu'un melon éclate, on ne peut pas le réutiliser. 
Le melon a autant de chance d'éclater en tombant de l'étage 1 que de l'étage 300, 
mais les trois melons vont toujours éclater en tombant à partir du même étage pour le même bâtiment.

### Instructions
Pour réussir cette énigme, vous devez répondre aux deux questions suivantes dans ce README.

* Quelle stratégie serait à employer afin de minimiser le nombre de fois qu'on lance un melon d'eau 
par la fenêtre afin de trouver le premier étage à partir duquel les melons éclatent?
* Quel est le nombre minimum de lancer de melons pour trouver cet étage?

N'oubliez pas que vous avez seulement trois melons et qu'il y a 300 étages!

### Indice
Commencez par trouver une stratégie qui fonctionne avec moins de melons sur un bâtiment avec moins d'étages. 
Appliquez le même raisonnement pour un problème avec 3 melons pour 300 étages.

### Réponse
Écrivez votre réponse ici:

Strategie on va lancer le melon tout les 4 etages avec la logique suivante : 
si il n'eclate pas on continu
si il eclate on descend de  2 etages et on lance un deuxieme melon
	Si il eclate on descend de  1 etages et on lance un troisieme melon 
		si il eclate alors cet etage et celui cherche 
		sinon c'est celui du dessus
	Sinon on monte de 1 etages et on lance un troisieme melon 
		si il eclate alors cet etage et celui cherche 
		sinon c'est celui du dessus
		

Avec cette strategie si n est le premier etage a partir duquel le melon eclate, on le trouvera en abs(n/4) + 3 lance de melon
soit dans notre ca au mieux 3 et au pire 78 lancer